<?php namespace XoneFobic\EventGenerator;

use ReflectionClass;

/**
 * Class EventListener
 *
 * @package XoneFobic\EventGenerator
 */
class EventListener {

    /**
     * Handle events given to the EventListener
     *
     * @param $event
     *
     * @return mixed
     */
    public function handle($event)
    {
        $eventName = $this->getEventName($event);

        if ($this->listenerIsRegistered($eventName))
        {
            return call_user_func([$this, 'when' . $eventName], $event);
        }
    }

    /**
     * Get Classname without namespace using the ReflectionClass
     *
     * @param $event
     *
     * @return string
     */
    protected function getEventName($event)
    {
        return (new ReflectionClass($event))->getShortName();
    }

    /**
     * Check if method generated exists
     *
     * @param $eventName
     *
     * @return bool
     */
    protected function listenerIsRegistered($eventName)
    {
        $method = "when{$eventName}";

        return method_exists($this, $method);
    }

} 

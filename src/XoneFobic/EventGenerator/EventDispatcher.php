<?php namespace XoneFobic\EventGenerator;

use Illuminate\Events\Dispatcher;
use Illuminate\Log\Writer;

/**
 * Class EventDispatcher
 *
 * @package XoneFobic\EventGenerator
 */
class EventDispatcher {

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $event;

    /**
     * @var \Illuminate\Log\Writer
     */
    private $log;

    /**
     * @param Dispatcher $event
     * @param Writer     $log
     */
    function __construct(Dispatcher $event, Writer $log)
    {
        $this->event = $event;
        $this->log = $log;
    }

    /**
     * Fire all pending events
     *
     * @param array $events
     */
    public function dispatch(array $events)
    {
        foreach ( $events as $event )
        {
            $eventName = $this->getEventName($event);
            $this->event->fire($eventName, $event);

            $this->log->info("$eventName was fired.");
        }
    }

    /**
     * Create eventName based on the namespaced class
     *
     * @param $event
     *
     * @return mixed
     */
    protected function getEventName($event)
    {
        return str_replace('\\', '.', get_class($event));
    }

} 
